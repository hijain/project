const express = require("express");
const router = express.Router();
const controller = require("../controller/file.controller");
const auth = require('../middleware/auth');

router.post("/post", auth.auth, controller.post);
router.post("/download/:name", auth.auth, controller.download);
router.get("/file/:name", auth.auth, controller.get);

router.get("/files/:name", auth.auth, controller.download);

router.post('/approvePost', auth.auth, function( req, res, next ) {
  if( auth.checkAuth( req, res ) && res.locals.user.IS_ADMIN ) {
    controller.approvePost( req, res, next );
  }
  else {
    res.redirect("/");
  }
});

router.post('/approveGroup', auth.auth, function( req, res, next ) {
    if( auth.checkAuth( req, res ) && res.locals.user.IS_ADMIN ) {
      controller.approveGroup( req, res, next );
    }
    else {
      res.redirect("/");
    }
});

router.post('/requestGroupAccess', auth.auth, function( req, res, next ) {
  if( auth.checkAuth( req, res ) ) {
    controller.getGroup( req.body.groupId, async function( group ) {
      if( group && group[0] && group[0].STATUS === 'ACTIVE' ) {
        await controller.groupMember( null, req.body.groupId, res.locals.user.USERNAME, 'PENDING' )
        res.redirect("/");
      }
      else {
        res.redirect("/");
      }
    } );
    
  }
  else {
    res.redirect("/");
  }
});

router.post('/approveGroupAccess', auth.auth, function( req, res, next ) {
  if( auth.checkAuth( req, res ) ) {
    controller.getGroup( req.body.groupId, async function( group ) {
      if( group && group[0] && group[0].STATUS === 'ACTIVE' && group[0].CREATED_BY === res.locals.user.USERNAME ) {
        await controller.groupMember( req.body.memberId, req.body.groupId, res.locals.user.USERNAME, req.body.state === 'approve' ? 'ACTIVE' : 'INACTIVE' )
        res.redirect("/groups");
      }
      else {
        res.redirect("/");
      }
    } );
    
  }
  else {
    res.redirect("/");
  }
});

router.post('/delete', auth.auth, controller.deletePost);

router.post("/group", auth.auth, controller.createGroup);

module.exports = router;
