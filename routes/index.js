var express = require("express");
var router = express.Router();
const auth = require("../middleware/auth");
const userController = require("../controller/user.controller");
const fileController = require("../controller/file.controller");

/* GET home page. */
router.get("/", function (req, res, next) {
  if (auth.checkAuth(req, res)) {
    if( res.locals.user.STATUS === 'INACTIVE' ) {
      req.session.user = undefined;
      res.render("errors", { message: "Your profile is deleted by admin" })
    }
    fileController.getWallPosts(function (posts) {                          //get all the posts to be rendered on home page
      posts = posts ? posts : [];
      posts = res.locals.user.STATUS === 'NEW' ? [] : posts;
      fileController.getWallGroups(res.locals.user.USERNAME, function (groups) {
        groups = groups ? groups : [];                                        //get all the groups to render hompage
        myGroupIds = {}
        for( const record of groups ) {
          if( record.MEMBER === res.locals.user.USERNAME && record.STATUS === 'ACTIVE' ) {
            myGroupIds[ record.GROUP_ID ] = true;
          }
        }
        // console.log( myGroupIds )
        posts = posts.filter( record => !record.NAME || myGroupIds[record.GROUP_ID] === true ) //filter posts based on the groups the user is part of or universal posts
        res.render("index", {
          is_admin: res.locals.user.IS_ADMIN,
          posts: posts,
          groups: groups,
          user: res.locals.user,
        });
      });
    });
  } else {
    res.redirect("/login");
  }
});

router.get("/login", function (req, res, next) {
  if (auth.checkAuth(req, res)) {
    res.redirect("/");
  } else {
    res.render("login");
  }
});

router.get("/post", auth.auth, function (req, res, next) {
  if (auth.checkAuth(req, res)) {
    
    fileController.getWallGroups(res.locals.user.USERNAME, function (groups) {      //get all the groups
      groups = groups ? groups : [];
      const myGroups = groups.filter( group => group.MEMBER === res.locals.user.USERNAME && group.STATUS === 'ACTIVE' ); // filter based on the groups the user is part of
      res.render("post", {myGroups: myGroups, 
        user: res.locals.user, is_admin: res.locals.user.IS_ADMIN});
    });
  } else {
    res.redirect("/login");
  }
});

router.get("/groups", auth.auth, function (req, res, next) {        // all the groups created by user
  if (auth.checkAuth(req, res)) {
    fileController.getGroupRequests( res.locals.user.USERNAME, function( groups ) {
      res.render("groups", {groups: groups, user: res.locals.user, is_admin: res.locals.user.IS_ADMIN});
    } );
  } else {
    res.redirect("/login");
  }
});

router.get("/group", auth.auth, function (req, res, next) {
  if (auth.checkAuth(req, res)) {
    res.render("group", {
      user: res.locals.user, is_admin: res.locals.user.IS_ADMIN});
  } else {
    res.redirect("/login");
  }
});

router.get("/admin", auth.auth, async function (req, res, next) {
  if (auth.checkAuth(req, res) && res.locals.user.IS_ADMIN) {
    userController.getAll(function (users) {
      users = users ? users : [];

      fileController.getPosts(function (posts) {
        posts = posts ? posts : [];

        fileController.getGroups(function (groups) {
          groups = groups ? groups : [];
          res.render("admin", {
            groups: groups,
            posts: posts,
            users: users,
            rootUser: res.locals.user,
            is_admin: res.locals.user.IS_ADMIN,
          });
        });
      });
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
