const uploadFile = require("../middleware/upload");
const fs = require("fs");

var mysql = require("mysql");
var config = require("../config/config");

// Use connection pools to improve performance
var pool = mysql.createPool(config.mysql);

const post = async (req, res) => {
  try {
    if( res.locals.user.STATUS != 'ACTIVE' ) {
      res.render("errors", {
        message: "Failed. Waiting for Admin to approve user",
        status: "500",
      });
      return;
    }
    const directoryPath = __basedir + "/resources/uploads/";
    let fileName = "";
    await uploadFile(req, res);

    if (req.file != undefined) {
      let currentFileName = req.file.originalname.split(".");
      const newFileName =
        res.locals.user.USERNAME +
        "_" +
        new Date().getTime() +
        "." +
        currentFileName[currentFileName.length - 1];
      fs.renameSync(
        directoryPath + req.file.originalname,
        directoryPath + newFileName
      );
      fileName = newFileName;
    }

    pool.getConnection(function (err, connection) {
      if (err) {
        res.render("errors", {
          message: "Failed to create a post",
          status: "500",
        });
        return;
      }
      var param = req.body;
      param.content = param.content.replace(/[#\$%\^\&*\)\(\\\/\:'"`+=-]/gi, '');
      param.name = param.name.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
      param.group = param.group.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');

      
      if (param.content.length < 3 || param.content.length > 500 ||param.name.length < 3 || param.name.length > 60 || param.group.length > 128 ) {
        res.render("errors", {
          message: "Failed to create a post",
          status: "500",
        });
        return;
      }
      const query = `INSERT INTO POST(created_by, name, content, file, status, created_at, group_id) VALUES('${
        res.locals.user.USERNAME
      }' , '${param.name}' , '${param.content}', '${fileName}', 'PENDING', '${new Date()
        .toISOString()
        .slice(0, 19)
        .replace("T", " ")}', ${
        param.group.length > 0 ? param.group : "NULL"
      })`;
      connection.query(query, function (err, result) {
        if (err) {
          res.render("errors", {
            message: "Failed to create a post",
            status: "500",
          });
          return;
        }
        res.redirect("/");
      });
    });
  } catch (err) {
    res.render("errors", { message: "Failed to create a post", status: "500" });
    
    return;
  }
};

const createGroup = async (req, res) => {
  try {
    
    if( res.locals.user.STATUS != 'ACTIVE' ) {
      res.render("errors", {
        message: "Failed. Waiting for Admin to approve user",
        status: "500",
      });
      return;
    }
    pool.getConnection(function (err, connection) {
      if (err) throw err;
      var param = req.body;
      
      param.name = param.name.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
      const query = `INSERT INTO GROUPS(created_by, name, status, created_at) VALUES('${
        res.locals.user.USERNAME
      }' , '${param.name}', 'PENDING', '${new Date()
        .toISOString()
        .slice(0, 19)
        .replace("T", " ")}')`;
      connection.query(query, async function (err, result) {
        if (err) throw err;
        await groupMember(
          null,
          result.insertId,
          res.locals.user.USERNAME,
          "ACTIVE"
        );
        res.redirect("/");
      });
    });
  } catch (err) {
    if (err) {
      res.render("errors", {
        message: "Failed to create a group",
        status: "500",
      });
    }
  }
};

const groupMember = async (membershipId, groupId, member, status) => {
  var query = "";

  if (membershipId) {
    query = `UPDATE GROUP_MEMBERS SET STATUS = '${status}' WHERE MEMBER_ID = ${membershipId}`;
  } else {
    query = `INSERT INTO GROUP_MEMBERS(GROUP_ID, MEMBER, STATUS, CREATED_AT) VALUES(${groupId}, '${member}', '${status}', '${new Date()
      .toISOString()
      .slice(0, 19)
      .replace("T", " ")}')`;
  }

  try {
    pool.getConnection(function (err, connection) {
      connection.query(query, function (err, result) {
        if (err) throw err;
        else return true;
      });
    });
  } catch (err) {
    return false;
  }
};

const getGroups = async (next) => {
  pool.getConnection(function (err, connection) {
    const query = `SELECT * FROM GROUPS`;
    connection.query(query, function (err, result) {
      connection.release();
      next(result);
    });
  });
};

const getGroup = async (groupId, next) => {
  pool.getConnection(function (err, connection) {
    const query = `SELECT * FROM GROUPS WHERE GROUP_ID = ${groupId}`;
    connection.query(query, function (err, result) {
      connection.release();
      next(result);
    });
  });
};

const getWallGroups = async (memberId, next) => {
  pool.getConnection(function (err, connection) {
    const query = `SELECT G.GROUP_ID, G.NAME, GM.MEMBER_ID, GM.MEMBER, GM.STATUS, G.CREATED_BY FROM GROUPS G
        LEFT OUTER JOIN GROUP_MEMBERS GM ON G.GROUP_ID = GM.GROUP_ID AND GM.MEMBER = '${memberId}'
        WHERE G.STATUS = 'ACTIVE'`;
    connection.query(query, function (err, result) {
      connection.release();
      next(result);
    });
  });
};

const getWallPosts = async (next) => {
  pool.getConnection(function (err, connection) {
    const query = `SELECT P.POST_ID, P.NAME AS POST_NAME, P.STATUS, P.CREATED_BY, G.NAME, G.GROUP_ID, P.CONTENT, P.FILE FROM POST P LEFT OUTER JOIN GROUPS G ON G.GROUP_ID = P.GROUP_ID WHERE  P.STATUS = 'ACTIVE'`;

    connection.query(query, function (err, result) {
      connection.release();
      next(result);
    });
  });
};

const getGroupRequests = async (username, next) => {
  pool.getConnection(function (err, connection) {
    const query = `SELECT G.NAME AS GROUP_NAME, G.GROUP_ID AS GROUP_ID, GM.MEMBER_ID AS MEMBERSHIP_ID, GM.MEMBER AS MEMBER, GM.STATUS AS MEMBERSHIP_STATUS, G.STATUS AS GROUP_STATUS FROM GROUP_MEMBERS GM
      RIGHT JOIN GROUPS G ON G.GROUP_ID = GM.GROUP_ID WHERE G.CREATED_BY = '${username}'`;
    connection.query(query, function (err, result) {
      connection.release();
      let myResult = {};
      if (result) {
        for (const record of result) {
          if (myResult[record.GROUP_ID] === undefined) {
            myResult[record.GROUP_ID] = {
              GROUP_ID: record.GROUP_ID,
              GROUP_NAME: record.GROUP_NAME,
              GROUP_STATUS: record.GROUP_STATUS,
              MEMBERS: [],
            };
          }

          if (record.MEMBERSHIP_ID) {
            myResult[record.GROUP_ID].MEMBERS.push({
              MEMBERSHIP_ID: record.MEMBERSHIP_ID,
              MEMBER: record.MEMBER,
              MEMBER_STATUS: record.MEMBERSHIP_STATUS,
            });
          }
        }
        // console.log(myResult);
        next(Object.values(myResult));
      } else next([]);
    });
  });
};

const getPosts = async (next) => {
  pool.getConnection(function (err, connection) {
    const query = `SELECT P.POST_ID, P.NAME AS POST_NAME, P.STATUS, P.CREATED_BY, G.NAME, P.CONTENT, P.FILE FROM POST P LEFT OUTER JOIN GROUPS G ON G.GROUP_ID = P.GROUP_ID`;
    connection.query(query, function (err, result) {
      connection.release();
      next(result);
    });
  });
};

const download = (req, res) => {
  const fileName = req.params.name;
  const directoryPath = __basedir + "/resources/uploads/";

  res.download(directoryPath + fileName, fileName, (err) => {
    if (err) {
      res.render("errors", {
        message: "Failed to download the file",
        status: "500",
      });
    }
  });
};

const get = (req, res) => {
  try{
    const fileName = req.params.name;
    const directoryPath = __basedir + "/resources/uploads/";
    res.sendFile(directoryPath + fileName);
  }
  catch (e) {
    res.render("errors", {
      message: "Failed to get file",
      status: "500",
    });
  }
};

const approvePost = (req, res, next) => {
  pool.getConnection(function (err, connection) {
    if(err) res.render("errors", { message: "Operation Failed" })
    
    var post_id = req.body.post_id.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
    var state = req.body.state.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
    const query = `UPDATE POST SET STATUS = '${
      state === "approve" ? "ACTIVE" : "INACTIVE"
    }' WHERE POST_ID LIKE '${post_id}'`;
    connection.query(query, function (err, result) {
      if(err) res.render("errors", { message: "Operation Failed" })
      connection.release();

      res.redirect("/admin");
    });
  });
};

const approveGroup = (req, res, next) => {
  pool.getConnection(function (err, connection) {
    var groupId = req.body.groupId.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
    var state = req.body.state.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
    if(err) res.render("errors", { message: "Operation Failed" })
    const query = `UPDATE GROUPS SET STATUS = '${
      state === "approve" ? "ACTIVE" : "INACTIVE"
    }' WHERE GROUP_ID LIKE '${groupId}'`;
    connection.query(query, function (err, result) {
      if(err) res.render("errors", { message: "Operation Failed" })
      connection.release();

      res.redirect("/admin");
    });
  });
};

const deletePost = (req, res, next) => {
  pool.getConnection(function (err, connection) {
    if(err) res.render("errors", { message: "Operation Failed" })
    
    var postId = req.body.postId.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
    const query = `UPDATE POST SET STATUS = 'INACTIVE' WHERE POST_ID LIKE '${postId}' AND CREATED_BY LIKE '${res.locals.user.USERNAME}'`;
    connection.query(query, function (err, result) {
      if(err) res.render("errors", { message: "Operation Failed" })
      connection.release();
      res.redirect("/");
    });
  });
};

module.exports = {
  post,
  getPosts,
  getWallPosts,
  download,
  approvePost,
  approveGroup,
  get,
  deletePost,
  createGroup,
  getGroups,
  getGroup,
  getWallGroups,
  groupMember,
  getGroupRequests,
};
