// Implementing Interaction with MySQL
var mysql = require("mysql");
var config = require("../config/config");

// Use connection pools to improve performance
var pool = mysql.createPool(config.mysql);

module.exports = {
  create: function (req, res, next) {
    pool.getConnection(function (err, connection) {
      var param = req.body;
        param.username = param.username.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
        param.name = param.name.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
        // console.log( param.username.length < 3 , param.username.length > 16 , param.password.length < 8 , param.password.length > 30 , param.name.length < 3 , param.name.length > 80 , param.password != param.password2 )
        if( param.username.length < 3 || param.username.length > 16 || param.password.length < 8 || param.password.length > 30 || param.name.length < 3 || param.name.length > 80 || param.password != param.password2 ) {
          res.render("errors", {
            message: "Failed to create an account",
            status: "500",
          });
          return;
        }

      const query = `INSERT INTO PROFILE(username, password, name, is_admin, status, created_at) VALUES('${
        param.username
      }' , SHA2('${param.password}', 224), '${
        param.name
      }', false, 'NEW', '${new Date()
        .toISOString()
        .slice(0, 19)
        .replace("T", " ")}')`;
      connection.query(query, function (err, result) {
        if (err) {
          res.render("errors", {
            message: "Failed to create an account",
            status: "500",
          });
        }
        if (result) {
          next();
        } else {
          res.render("errors", {
            message: "Failed to create an account",
            status: "500",
          });
        }
        connection.release();
      });
    });
  },
  login: function (req, res, next) {
    pool.getConnection(function (err, connection) {
      var param = req.body;
      
      param.username = param.username.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
      if (param.username.length < 3 || param.username.length > 16 || param.password.length < 8 || param.password.length > 30) {
        res.render("errors", {
        message: "Failed to login",
        status: "500",
          });
        return;
      } 
      const query = `SELECT * FROM PROFILE WHERE STATUS != 'INACTIVE' AND USERNAME LIKE '${param.username}' AND PASSWORD = SHA2('${param.password}', 224)`;

      connection.query(query, function (err, result) {
        if (err) {
          res.render("errors", { message: "Failed to login", status: "500" });
        }
        // console.log(result);
        if (result && result.length > 0) {
          req.session.user = { ...result[0], expires: new Date() };
          res.redirect("/");
        } else {
          res.render("errors", { message: "Failed to login", status: "500" });
        }
        connection.release();
      });
    });
  },
  getAll: function (next) {
    pool.getConnection(function (err, connection) {
      const query = `SELECT USERNAME, NAME, STATUS, CREATED_AT FROM PROFILE`;
      connection.query(query, function (err, result) {
        connection.release();
        next(result);
      });
    });
  },
  approve: function (req, res, next) {
    pool.getConnection(function (err, connection) {
      let username = req.body.username.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
      let state = req.body.state.replace(/[!@#\$%\^\&*\)\(\\\/\:'`+=._-]/gi, '');
      const query = `UPDATE PROFILE SET STATUS = '${
        state === "approve" ? "ACTIVE" : "INACTIVE"
      }' WHERE USERNAME LIKE '${username}'`;
      connection.query(query, function (err, result) {
        if (err) {
          res.render("errors", {
            message: "Failed to change user status",
            status: "500",
          });
        }
        connection.release();

        res.redirect("/admin");
      });
    });
  },
};
