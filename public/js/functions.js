function removeChar(that, status = true) {
    var a = document.getElementById(that.id).value;
    if( status === true )
        a = a.replace(/[!@#\$%\^\&*\)\(\\\/\:+='"`._-]/gi, '');
    else 
        a = a.replace(/[#\$%\^\&*\)\(\\\/\:+'"`=-]/gi, '');
    document.getElementById(that.id).value = a;
}

function checkSpcialChar(event){
    if(((event.keyCode >= 33) && (event.keyCode <= 47) || (event.keyCode >= 58) && (event.keyCode <= 64) || (event.keyCode >= 91) && (event.keyCode <= 96))){
       event.returnValue = false;
       return;
    }
    event.returnValue = true;
 }