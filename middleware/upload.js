const util = require("util");
const multer = require("multer");
const path = require("path");
const maxSize = 2 * 1024 * 1024;

let suppotedFileTypes = [".jpg",
".jpeg",
".png",
".gif",
".ico",
".txt",
".mp4",
".m4v",
".mov",
".wmv",
".avi",
".mpg",
".ogv",
".3gp",
".3g2",
".pdf",
".doc",
".ppt" ,
".pptx",
".pps",
".ppsx",
".docx",
".odt",
".xls" ,
".xlsx",
".psd",
".mp3",
".m4a",
".ogg",
".wav"]

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __basedir + "/resources/uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

let uploadFile = multer({
  storage: storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if( !suppotedFileTypes.includes( ext ) ) {
        return callback(new Error('File Type not supported'))
    }
    callback(null, true)
  },
  // limits:{
  //     fileSize: 1024 * 1024
  // }
  limits: { fileSize: maxSize },
}).single("file");

let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = uploadFileMiddleware;