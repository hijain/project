const generateToken = (req, res, next) => {
  req.session.user = {...req.session.user, expires: new Date()};
  next( );
}

const checkAuth =  ( req, res ) => {
  try {
    if( req.session.user ) {
      // const token = req.cookies.auth;
      res.locals.user = req.session.user;
      return true;
    }
    else {
      return false;
    }
  }
  catch {
    return false;
  }
}

module.exports = {
  checkAuth: (req, res) => checkAuth( req, res ),
  auth: (req, res, next) => {
  try {
    const status = checkAuth( req, res );
    if( status ) {
      generateToken( req, res, next );
    }
    else {
      throw 'User logged out!';
    }
  } catch {
    res.redirect('/login')
  }}
};